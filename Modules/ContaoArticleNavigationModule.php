<?php
namespace Blockbrew\Bundle\ContaoArticleNavigationBundle\Modules;

use Blockbrew\Bundle\ContaoArticleNavigationBundle\Models\ContaoArticleNavigationModel;

class ContaoArticleNavigationModule extends \Module
{
    protected $strTemplate = 'nav_article';

    public function generate()
    {
        if (TL_MODE == 'BE') {
            $objTemplate = new \BackendTemplate('be_wildcard');
            $objTemplate->wildcard = '### '
                . utf8_strtoupper($GLOBALS['TL_LANG']['MOD']['event_filter'][0])
                . ' ###';
            $objTemplate->id = $this->id;
            return $objTemplate->parse();
        }
        return parent::generate();
    }

    protected function compile()
    {
        /** @var \PageModel $objPage */
        global $objPage;

        // Set the trail and level
        if ($this->defineRoot && $this->rootPage > 0)
        {
            $trail = [$this->rootPage];
        }
        else
        {
            $trail = $objPage->trail;
        }

        $level = 0;

        $navItems = $this->getNavItems([], $trail[0], $objPage->trail, $this->nesting, $level, $this->showHidden, $this instanceof \ModuleSitemap);

        $this->Template->request = ampersand(\Environment::get('indexFreeRequest'));
        $this->Template->skipId = 'skipNavigation' . $this->id;
        $this->Template->skipNavigation = specialchars($GLOBALS['TL_LANG']['MSC']['skipNavigation']);
        $this->Template->items = $this->renderArticleNav($trail[0], 0, $navItems);
    }

    protected function getArticleItems($refPage)
    {
        $articles = ContaoArticleNavigationModel::findPublishedNavByPidAndColumn($refPage, $this->inColumn, []);

        $articleItems = [];

        if ($articles && (0 < $articles->count())) {
            $page = \PageModel::findById($refPage);
            $baseHref = $this->getHref($page);

            foreach ($articles as $article) {
                $articleItems[] = [
                    'id' => $article->id,
                    'link' => $article->title,
                    'trail' => false,
                    'href' => $baseHref . '#' . $article->alias,
                    'subItems' => ''
                ];
            }
        }

        return $articleItems;
    }

    protected function getNavItems($navItems, $refPage, $trail, $nesting, $level, $showHidden, $sitemap)
    {
        if ($level < $nesting) {
            $pages = \PageModel::findPublishedSubpagesWithoutGuestsByPid($refPage, $showHidden, $sitemap);

            $level++;

            if ($pages && (0 < $pages->count())) {
                foreach ($pages as $page) {

                    $href = $this->getHref($page);
                    $inTrail = false;

                    foreach ($trail as $element) {
                        if ($page->id == $element) {
                            $inTrail = true;
                        }
                    }

                    $navItems[] = [
                        'id' => $page->id,
                        'link' => $page->title,
                        'href' => $href,
                        'trail' => $inTrail,
                        'subItems' => $this->getNavItems($navItems[$level], $page->id, $trail, $nesting, $level, $showHidden, $sitemap),
                    ];
                }
            } else {
                return $this->getArticleItems($refPage);
            }
        } else {
            return $this->getArticleItems($refPage);
        }

        return $navItems;
    }

    protected function getHref($page)
    {
        $href = null;

        // Get href
        switch ($page->type)
        {
            case 'redirect':
                $href = $page->url;

                if (strncasecmp($href, 'mailto:', 7) === 0)
                {
                    $href = \StringUtil::encodeEmail($href);
                }
                break;

            case 'forward':
                if ($page->jumpTo)
                {
                    /** @var \PageModel $objNext */
                    $objNext = $page->getRelated('jumpTo');
                }
                else
                {
                    $objNext = \PageModel::findFirstPublishedRegularByPid($page->id);
                }

                // Hide the link if the target page is invisible
                if ($objNext === null || !$objNext->published || ($objNext->start != '' && $objNext->start > time()) || ($objNext->stop != '' && $objNext->stop < time()))
                {
                    continue;
                }

                $href = $objNext->getFrontendUrl();
                break;

            default:
                $href = $page->getFrontendUrl();
                break;
        }

        return $href;
    }

    protected function renderArticleNav($pid, $level, $navItems)
    {
        /** @var \FrontendTemplate|object $objTemplate */
        $objTemplate = new \FrontendTemplate('nav_article');

        $objTemplate->pid = $pid;
        $objTemplate->type = get_class($this);
        $objTemplate->level = $level++;

        if ($navItems) {
            foreach ($navItems as $navItem) {
                $subItems = '';
                if ($navItem['subItems'] && ('' != $navItem['subItems'])) {
                    $subItems = $this->renderArticleNav($pid, $level, $navItem['subItems']);
                }

                $items[] = [
                    'trail' => $navItem['trail'],
                    'link' => $navItem['link'],
                    'href' => $navItem['href'],
                    'subItems' => $subItems
                ];

            }
            
            $objTemplate->items = $items;

            return !empty($items) ? $objTemplate->parse() : '';
        }
    }
}