<?php

namespace Blockbrew\Bundle\ContaoArticleNavigationBundle\Models;

class ContaoArticleNavigationModel extends \ArticleModel
{

    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_article';



    /**
     * Find all published articles by their parent ID and column
     *
     * @param integer $intPid     The page ID
     * @param string  $strColumn  The column name
     * @param array   $arrOptions An optional options array
     *
     * @return \Model\Collection|\ArticleModel[]|\ArticleModel|null A collection of models or null if there are no articles in the given column
     */
    public static function findPublishedNavByPidAndColumn($intPid, $strColumn, array $arrOptions=array())
    {
        $t = static::$strTable;
        $arrColumns = array("$t.pid=? AND $t.inColumn=? AND $t.showInNav='1'");
        $arrValues = array($intPid, $strColumn);

        if (!BE_USER_LOGGED_IN)
        {
            $time = \Date::floorToMinute();
            $arrColumns[] = "($t.start='' OR $t.start<='$time') AND ($t.stop='' OR $t.stop>'" . ($time + 60) . "') AND $t.published='1' AND $t.showInNav='1'";
        }

        if (!isset($arrOptions['order']))
        {
            $arrOptions['order'] = "$t.sorting";
        }

        return static::findBy($arrColumns, $arrValues, $arrOptions);
    }
}
