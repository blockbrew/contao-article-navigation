<?php

// Article Nav
$GLOBALS['TL_DCA']['tl_article']['palettes']['default'] = str_replace(
    'teaser;',
    'teaser;{nav_legend},showInNav;',
    $GLOBALS['TL_DCA']['tl_article']['palettes']['default']
);

$GLOBALS['TL_DCA']['tl_article']['fields'] += [
    'showInNav' => [
        'label'                   => &$GLOBALS['TL_LANG']['tl_article']['showInNav'],
        'exclude'                 => true,
        'inputType'               => 'checkbox',
        'eval'                    => array('tl_class'=>'w50 m12'),
        'sql'                     => "char(1) NOT NULL default '1'"
    ]
];
