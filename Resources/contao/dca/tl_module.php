<?php

// Article Nav
$GLOBALS['TL_DCA']['tl_module']['palettes'] += [
    'article-nav' => '{title_legend},name,type;{reference_legend:hide},defineRoot,showHidden;{nav_legend},inColumn,nesting;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID,space'
];

$GLOBALS['TL_DCA']['tl_module']['fields'] += [
    'nesting' => [
        'label'                   => &$GLOBALS['TL_LANG']['tl_module']['nesting'],
        'exclude'                 => true,
        'inputType'               => 'text',
        'eval'                    => array('maxlength'=>2, 'rgxp'=>'natural', 'tl_class'=>'w50'),
        'sql'                     => "smallint(5) unsigned NOT NULL default '0'"
    ]
];
